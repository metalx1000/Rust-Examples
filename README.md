# Rust-Examples

Copyright Kris Occhipinti 2023-02-13

(https://filmsbykris.com)

License GPLv3

# Setup on Linux (Debian Based)
```bash
sudo apt install rustc cargo
```

# Compiling
- To compile a single example
```bash
rustc code.rs

#or using cargo
cargo clean
cargo run
cargo build
```

- To compile all examples
```bash
./compile.sh
```
