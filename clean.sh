#!/bin/bash

start="$PWD"

find -name "Cargo.toml"|while read f
do
  d="$(dirname "$f")"
  echo "Cleaning $d..."
  cd "$d"
  cargo clean
  cd "$start"
done
