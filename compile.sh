#!/bin/bash
###################################################################### 
#Copyright (C) 2023  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

start="$PWD"
mkdir -p bin

echo "Compiling..."
for i in src/*.rs;
do 
  echo "$i";
  rustc "$i" --out-dir bin
done

echo -e "\n\nCompiled Files:"
find bin -type f


find -name "Cargo.toml"|while read f
do
  d="$(dirname "$f")"
  echo "Moving into $d..."
  cd "$d"
  cargo build
  cd "$start"
done
