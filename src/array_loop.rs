fn main() {
    let distros = ["Debian", "Ubuntu", "Gentoo", "Arch"];

    println!("Some Linux Distros Are:");
    for i in 0..distros.len() {
        println!("{}", distros[i]);
    }

    println!();
    println!("Number of Distros listed: {}", distros.len());
}
