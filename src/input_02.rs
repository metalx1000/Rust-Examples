use std::io;
use std::io::Write; // <--- bring flush() into scop

fn main() {
    print!("Enter a name: ");
    io::stdout().flush().unwrap();
    let mut name = String::new();

    io::stdin().read_line(&mut name).expect("failed to readline");
    print!("You entered {}", name);
}
