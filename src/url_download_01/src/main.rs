use std::collections::HashMap;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let resp = reqwest::get("https://filmsbykris.com/scripts/2023/hello.html")
        .await?;
    println!("{:#?}", resp);
    Ok(())
}
